import dangeon.WalkInLabyrinth;
import fighters.ArenaFighters;
import fighters.dragonRiders.DragonRider;
import fighters.dragons.Dragon;
import fighters.knights.BlackKnight;
import fighters.knights.HollyKnight;
import fighters.knights.Knight;
import healers.Healer;
import maze_bfs.IMaze;
import maze_bfs.MazeBFS;

public class Main {
    public static void main(String[] args) {

        WalkInLabyrinth freddy = new Dragon("Freddy", 10000, 150, 0.5f, Dragon.WATER | Dragon.WIND);
        Dragon chucky = new Dragon("Chucky", 300, 14, 0.3f, Dragon.EARTH | Dragon.FIRE);
        Dragon antaras = new Dragon("Antaras", 400, 10, 0.4f, Dragon.EARTH | Dragon.WIND);
        Dragon valakas = new Dragon("Valakas", 400, 10, 0.4f, Dragon.EARTH | Dragon.WIND);
        DragonRider jonSnow = new DragonRider("JonSnow", 100, 10, 5);
        Knight artur = new Knight("Artur", 100, 40.0f, 0.5f, 1);
        ArenaFighters hollyKnight = new HollyKnight("HollyKnight", 100, 15, 0.15f, 2, 5);
        ArenaFighters hellKnight = new BlackKnight("HellKnight", 150, 40, 0.5f, 1);
        Healer healer = new Healer("Bishop", 5);

        IMaze mazeBFS = new MazeBFS(freddy, chucky, valakas);
        mazeBFS.startWay();

//        Labyrinth labyrinth = new Labyrinth(10, 10, freddy, chucky, valakas);
//        labyrinth.startWalk();



//        DuelArena duelArena = new DuelArena(healer, antaras, chucky, 5);
//        duelArena.setGodHand((member1, member2) -> {
//            float dam = valakas.attack(member2);
//            System.out.println("valakas attack " + member2.getName() + " get damage " + dam);
//            return false;
//        });
//        duelArena.startBattle();
//        duelArena.printWinner();
//
//
//        Tournament tournament = new Tournament(healer, antaras, jonSnow, hollyKnight, hellKnight, freddy, chucky,
//                artur, valakas, 5);
//        tournament.startBattle();
//        tournament.printWinner();



    }
}
