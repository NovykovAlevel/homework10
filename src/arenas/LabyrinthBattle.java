package arenas;

import dangeon.FindEnemy;
import dangeon.WalkInLabyrinth;
import fighters.ArenaFighters;

public class LabyrinthBattle extends DuelArena implements FindEnemy {
    public LabyrinthBattle() {
        super(null, null, null, 0);
    }

    @Override
    public void startBattle() {
        setStartRound(true);
        while (isFightContinue(member1, member2)) {
            fight(member1, member2);
        }
    }

    @Override
    public void battle(WalkInLabyrinth walkInLabyrinth, ArenaFighters member) {
        if (walkInLabyrinth instanceof ArenaFighters) {
            member1 = (ArenaFighters) walkInLabyrinth;
            member2 = member;
            startBattle();
            printWinner();
        }
    }
}
