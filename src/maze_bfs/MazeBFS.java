package maze_bfs;


import dangeon.Labyrinth;
import dangeon.WalkInLabyrinth;
import fighters.ArenaFighters;


import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class MazeBFS extends Labyrinth {
    private static final int MIN_HEIGHT = 20;
    private static final int MIN_WIDTH = 20;

    private static int amountBarrier = 50;
    private static int amountPotionRecovery = 3;
    private static int amountElixirPoison = 3;
    private Queue<Node> listAccessibleNode;
    private Set<Node> listTraversedNodes;
    private int[][] neighboringCoordinates = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    public MazeBFS(WalkInLabyrinth member, ArenaFighters... enemies) {
        super(MIN_HEIGHT, MIN_WIDTH,amountBarrier, amountPotionRecovery, amountElixirPoison, member, enemies );
        listAccessibleNode = new LinkedList<>();
        listAccessibleNode.add(new Node(memberCoordinateY, memberCoordinateX));
        listTraversedNodes = new LinkedHashSet<>();
    }

    @Override
    public void startWay() {
        while (!isGameOver()) {
            seeMap();
            movementMember();
            markWay();
        }
        System.out.printf("Traveled distance of the member:\n %s,", listTraversedNodes.toString());
    }

    @Override
    protected void movementMember() {
        if (listAccessibleNode.size() > 0) {
            Node currentCell = listAccessibleNode.poll();
            memberCoordinateY = currentCell.getY();
            memberCoordinateX = currentCell.getX();
            eventsOnWay();
            searchAlternativeWays(memberCoordinateY, memberCoordinateX);
            listTraversedNodes.add(currentCell);
            announcement(memberCoordinateY, memberCoordinateX);
        }
    }


    private void searchAlternativeWays(int memberCoordinateY, int memberCoordinateX) {
        for (int[] alternativeCoordinate : neighboringCoordinates) {
            int newCoordinatesY = memberCoordinateY + alternativeCoordinate[0];
            int newCoordinatesX = memberCoordinateX + alternativeCoordinate[1];
            if (isSuccessfulMovement(newCoordinatesY, newCoordinatesX)) {
                Node node = new Node(newCoordinatesY, newCoordinatesX);
                if (!listAccessibleNode.contains(node) && !listTraversedNodes.contains(node))
                    listAccessibleNode.add(node);
            }
        }
    }

    private void announcement(int memberCoordinateY, int memberCoordinateX) {
        System.out.printf("The participant is at the mark [%d,%d]. Nodes left: %d. Amount of remaining HP %d.\n",
                memberCoordinateY,memberCoordinateX, counterToWin,(int) member.getHP());
    }
}