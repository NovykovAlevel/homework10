package maze_bfs;

import java.util.Objects;

public class Node {
    private int y;
    private int x;

    public Node(int y, int x) {
        this.y = y;
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node cell = (Node) o;
        return getY() == cell.getY() &&
                getX() == cell.getX();
    }

    @Override
    public String toString() {
        return "Node[" + y + ":" + x +"]\n";
    }

    @Override
    public int hashCode() {
        return Objects.hash(getY(), getX());
    }
}
