package dangeon;


import arenas.LabyrinthBattle;
import maze_bfs.IMaze;
import potions.PotionPoison;
import potions.PotionRecovery;
import fighters.ArenaFighters;

import java.util.Arrays;

public abstract class Labyrinth implements IMaze {
    protected char iconEmptySquare = '\u2610';
    protected char iconLabeledSquare = '\u25A9';
    protected char iconFinish = '\u2FA8';
    protected char iconBarrier = '\u25CF';
    protected char iconSmilingSmiley = '\u263B';
    protected char iconPotionRecovery = '\u271a';
    protected char iconPotionPoison = '\u2620';
    protected char iconEnemy = '\u265e';
    protected int memberCoordinateY = 0;
    protected int memberCoordinateX = 0;
    protected int finishCoordinateY;
    protected int finishCoordinateX;
    protected int counterToWin;
    protected char[][] labyrinth;
    protected ArenaFighters[] enemies;
    protected WalkInLabyrinth member;

    protected int amountBarrier;
    protected int amountPotionRecovery;
    protected int amountPotionPoison;
    protected int amountEnemy;
    protected int numEnemy = 0;


    public Labyrinth(int height, int width,int amountBarrier,int amountPotionRecovery,
                    int amountPotionPoison, WalkInLabyrinth member, ArenaFighters... enemies) {
        labyrinth = new char[height][width];
        finishCoordinateY = height - 1;
        finishCoordinateX = width - 1;
        this.member = member;
        counterToWin = height * width - amountBarrier;
        this.enemies = enemies;
        amountEnemy = enemies.length;
        this.amountBarrier = amountBarrier;
        this.amountPotionRecovery = amountPotionRecovery;
        this.amountPotionPoison = amountPotionPoison;
        decorationLabyrinth();
    }
    protected abstract void movementMember();


    private void decorationLabyrinth() {
        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth[i].length; j++) {
                labyrinth[i][j] = iconEmptySquare;
            }
        }
        creatingObstacles();
        creatingFinish();
    }

    private void creatingObstacles() {
        creatingNumberOfDecorElements(amountBarrier, iconBarrier);
        creatingNumberOfDecorElements(amountPotionRecovery, iconPotionRecovery);
        creatingNumberOfDecorElements(amountPotionPoison, iconPotionPoison);
        creatingNumberOfDecorElements(amountEnemy, iconEnemy);
    }

    private void creatingNumberOfDecorElements(int num, char icon) {
        for (int i = 0; i < num; i++) {
            creatingOneElementOfDecor(icon);
        }
    }
    private void creatingOneElementOfDecor(char icon) {
        int y = (int) (Math.random() * (finishCoordinateY - 1) + 1);
        int x = (int) (Math.random() * (finishCoordinateX - 1) + 1);
        if (isIconInNode(y, x, iconEmptySquare)) {
            labyrinth[y][x] = icon;
        } else {
            creatingOneElementOfDecor(icon);
        }
    }

    private void creatingFinish() {
        if(dropTheCoin()) {
            labyrinth[finishCoordinateY][finishCoordinateX] = iconFinish;
        }
    }
    protected boolean isGameOver() {
        if (labyrinth[memberCoordinateY][memberCoordinateX] == iconFinish) {
            markWay();
            seeMap();
            System.out.printf("Member successfully comes to the Finish! Not visited cells %1$s", counterToWin);
            return true;
        } else if ((counterToWin == 0)) {
            markWay();
            seeMap();
            System.out.println("All nodes is over. Maze has no exit!");
            return true;
        } else if (member.getHP() <= 0) {
            System.out.println("Your hero is dead " + '\u2639');
            return true;
        }
        return false;
    }

    protected void markWay() {
        if (isReplaceIcon()) {
            labyrinth[memberCoordinateY][memberCoordinateX] = iconLabeledSquare;
            counterToWin();
        } else if (labyrinth[memberCoordinateY][memberCoordinateX] == iconFinish) {
            counterToWin();
            labyrinth[memberCoordinateY][memberCoordinateX] = iconSmilingSmiley;
        }
    }

    private boolean isReplaceIcon() {
        return isIconInNode(memberCoordinateY, memberCoordinateX, iconEmptySquare)
                || isIconInNode(memberCoordinateY, memberCoordinateX, iconPotionRecovery)
                || isIconInNode(memberCoordinateY, memberCoordinateX, iconPotionPoison)
                || isIconInNode(memberCoordinateY, memberCoordinateX, iconEnemy);
    }

    protected void seeMap() {
        for (char[] lab : labyrinth) {
            System.out.println(Arrays.toString(lab));
        }
    }

    private void counterToWin() {
        counterToWin--;
    }
    private boolean isSuccessfulMovement(int[][] coordinatesMovement) {
        if (coordinatesMovement == null || coordinatesMovement[0].length < 2) {
            return false;
        } else if ((coordinatesMovement[0][0] < labyrinth.length && coordinatesMovement[0][0] >= 0)
                && (coordinatesMovement[0][1] < labyrinth[0].length && coordinatesMovement[0][1] >= 0)) {
            return !(isIconInNode(coordinatesMovement[0][0], coordinatesMovement[0][1], iconBarrier));
        } else {
            return false;
        }
    }
    protected boolean isSuccessfulMovement(int coordinateY, int coordinateX) {
        int[][] coordinates = new int[][]{{coordinateY, coordinateX}};
        return isSuccessfulMovement(coordinates);
    }

    private boolean isIconInNode(int coordinateY, int coordinateX, char icon) {
        return labyrinth[coordinateY][coordinateX] == icon;
    }
    protected void eventsOnWay() {
        if (isIconInNode(memberCoordinateY, memberCoordinateX, iconPotionRecovery)) {
            openPotion(new PotionRecovery());
        } else if (isIconInNode(memberCoordinateY, memberCoordinateX, iconPotionPoison)) {
            openPotion(new PotionPoison());
        } else if (isIconInNode(memberCoordinateY, memberCoordinateX, iconEnemy)) {
            startBattle(new LabyrinthBattle());
        }
    }

    private void startBattle(FindEnemy enemy) {
        enemy.battle(member, enemies[numEnemy++]);
    }

    private void openPotion(FindPotion findPotion) {
        findPotion.open(member);
    }
    private boolean dropTheCoin() {
        int randomNum = (int) (Math.random() * 100);
        return (randomNum % 2) == 0;
    }
}
