package potions;

import dangeon.FindPotion;
import dangeon.WalkInLabyrinth;

public class PotionRecovery implements FindPotion {
    @Override
    public void open(WalkInLabyrinth member) {
        member.setHP(member.getMaxHP());
    }
}
