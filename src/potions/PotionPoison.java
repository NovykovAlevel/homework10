package potions;

import dangeon.FindPotion;
import dangeon.WalkInLabyrinth;

public class PotionPoison implements FindPotion {
    @Override
    public void open(WalkInLabyrinth member) {
        float damage = 0.3f * member.getHP();
        member.setHP(damage);
    }
}
